import './App.css';
import {useEffect, useState} from "react";

const TEXT_TO_COPY = 'I was put via JS into Clipboard';

const fallback = (text) => {
    const isIos = navigator.userAgent.match(/ipad|iphone/i);
    const textarea = document.createElement('textarea');

    // create textarea
    textarea.value = text;

    // ios will zoom in on the input if the font-size is < 16px
    textarea.style.fontSize = '20px';
    document.body.appendChild(textarea);

    // select text
    if (isIos) {
        const range = document.createRange();
        range.selectNodeContents(textarea);

        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        textarea.setSelectionRange(0, 999999);
    } else {
        textarea.select();
    }

    // copy selection
    const success = document.execCommand('copy');

    // cleanup
    document.body.removeChild(textarea);
    return success;
};

export const copyToClipboard = async (text) => {
    if (!navigator.clipboard) {
        return Promise.resolve(fallback(text));
    }
    return navigator.clipboard.writeText(text)
};

const requestPermission = async () => {
    const queryOpts = {name: 'clipboard-write', allowWithoutGesture: false};
    await navigator.permissions?.query(queryOpts);
}

function App() {
    const [done, setDone] = useState(false);
    useEffect(() => {
        requestPermission();
    }, [])

    const onclick = (() => {
        copyToClipboard(TEXT_TO_COPY).then(result => {
            setDone(true);
        })
    })

    return (
        <div>
            <p>
                <button onClick={onclick}>Click me to copy some text</button>
            </p>
            {done && <p>Copied <b>{TEXT_TO_COPY}</b>.</p>}
        </div>
    );
}

export default App;
