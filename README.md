#  PoC  Drag and Drop

Code is lying in src/App.js (it's a React JS App)

To develop:
```
npm install
npm run dev
```
To create a build
```
npm run build
```
To publish:
```
./scripts/build-and-deploy.sh
```
 * checkout:  https://yanick.dickbauer.at/poc-drag-and-drop/